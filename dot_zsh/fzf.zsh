export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
--color=fg:008,fg+:-1
--color=bg:-1,bg+:-1
--color=hl:002,hl+:002
--color=info:003
--color=prompt:004
--color=pointer:004
--color=marker:002
--color=spinner:003
--color=header:008'
