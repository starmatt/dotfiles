return {
  {
    "williamboman/mason.nvim",
    opts = function(_, opts)
      table.insert(opts.ensure_installed, "phpstan")
      table.insert(opts.ensure_installed, "pint")
    end,
  },
  {
    "nvimtools/none-ls.nvim",
    opts = function(_, opts)
      local nls = require("null-ls")

      opts.sources = vim.list_extend(opts.sources, {
        nls.builtins.formatting.pint,
        nls.builtins.diagnostics.phpstan.with({
          to_temp_file = false,
          method = nls.methods.DIAGNOSTICS_ON_SAVE,
        }),
      })
    end,
  },
}
