local wezterm = require("wezterm")

return {
	color_scheme = "tokyonight",
	-- dpi = 144.0,
	enable_wayland = true,
	exit_behavior = "Close",
	font = wezterm.font_with_fallback({ "Input Nerd Font", "JetBrains Mono" }),
	font_size = 10,
	hide_tab_bar_if_only_one_tab = true,
	inactive_pane_hsb = { saturation = 1.0, brightness = 0.5 },
	line_height = 1.2,
	tab_bar_at_bottom = true,
	use_fancy_tab_bar = false,
	warn_about_missing_glyphs = false,
}
