local wezterm = require('wezterm')

local map = function (mods, key, action)
    return { mods = mods, key = key, action = action }
end

local leader_map = function (key, action)
    return map('LEADER', key, action)
end

local action = wezterm.action

return {
    disable_default_key_bindings = true,
    leader = { mods = 'CTRL', key = 'a' },

    keys = {
        map('LEADER|CTRL', 'a', action({ SendString = '\x01' })),
        -- Increase font size
        map('ALT', '=', 'IncreaseFontSize'),
        -- Decrease font size
        map('ALT', '-', 'DecreaseFontSize'),
        -- Reset font size
        map('ALT', '0', 'ResetFontSize'),
        -- Reload configuration
        leader_map('r', 'ReloadConfiguration'),
        -- Zoom in on a pane
        leader_map('z', 'TogglePaneZoomState'),
        -- Spawn a new window
        leader_map('n', 'SpawnWindow'),
        -- Activate Copy Mode
        leader_map('x', 'ActivateCopyMode'),
        -- Copy
        leader_map('c', action({ CopyTo = 'Clipboard' })),
       -- & Paste
        leader_map('v', action({ PasteFrom = 'Clipboard' })),
        -- Open a new tab
        leader_map('t', action({ SpawnTab = 'CurrentPaneDomain' })),
        -- Open a new pane vertically
        leader_map('-', action({
            SplitVertical = { domain = 'CurrentPaneDomain' }
        })),
        -- Open a new pane horizontally
        leader_map('/', action({
            SplitHorizontal = { domain = 'CurrentPaneDomain' }
        })),
        -- Go to the pane on the left
        leader_map('h', action({ ActivatePaneDirection = 'Left' })),
        -- Go to the pane on the right
        leader_map('l', action({ ActivatePaneDirection = 'Right' })),
        -- Go one pane up
        leader_map('k', action({ ActivatePaneDirection = 'Up' })),
        -- Go one pane down
        leader_map('j', action({ ActivatePaneDirection = 'Down' })),
        -- Go to the previous tab
        leader_map(',', action({ ActivateTabRelative = -1 })),
        -- Go to the next tab
        leader_map('.', action({ ActivateTabRelative = 1 })),
    },
}
